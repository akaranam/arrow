using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class HandleData : MonoBehaviour
{   
    
    [SerializeField] private ToggleData _ToggleData = new ToggleData();

    public static ToggleData ReadFromJson() {
        string jsonString = File.ReadAllText("Assets/SettingsData.json"); 
        return JsonUtility.FromJson<ToggleData>(jsonString);
    } 
    
    public void SaveCompassPress() {
        ToggleData recentData = ReadFromJson(); 
        _ToggleData.persistentCompass = !recentData.persistentCompass;
        _ToggleData.handMap = recentData.handMap;
        string toggle = JsonUtility.ToJson(_ToggleData);
        System.IO.File.WriteAllText("Assets/SettingsData.json", toggle);
    }

    public void SaveMapPress() {
        ToggleData recentData = ReadFromJson(); 
        _ToggleData.persistentCompass = recentData.persistentCompass;
        _ToggleData.handMap = !recentData.handMap;
        string toggle = JsonUtility.ToJson(_ToggleData);
        System.IO.File.WriteAllText("Assets/SettingsData.json", toggle);
    }

}


[System.Serializable]
public class ToggleData {
    public bool persistentCompass;
    public bool handMap;
}