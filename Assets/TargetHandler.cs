using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TargetHandler : MonoBehaviour
{

    public GameObject navBeacon;
    public Transform user;
    public Transform target;
    public TelemetryStream telemetryData;

    public string label = "label";
    public GameObject display;

    private TextMeshPro distText;

    private float[] targetCoords; // target GPS
    // private float[] userCoords; // GPS from telemetry stream
    private float navThreshold = 0.5f;

    // Start is called before the first frame update
    void Start()
    {
        navBeacon.SetActive(false);
        distText = display.GetComponent<TextMeshPro>();
    }

    // Update is called once per frame
    void Update()
    {
        if(navBeacon.activeInHierarchy)
        {
            Vector3 relPosition = GPStoNav(targetCoords);
            float dist = relPosition.magnitude;

            if (dist > navThreshold) {
                target.position = user.position + relPosition;
                distText.text = label + (dist / 1.0f).ToString("0.0") + "m";
            } else {
                navBeacon.SetActive(false);
            }

        }
            
    }


    // Set navigation coordinates (GPS lat, lon)
    public void SetTargetCoords(float[] coordsGPS)
    {
        targetCoords = coordsGPS;
        // print(targetCoords);
    }

    public void SetTargetLabel(string newLabel)
    {
        label = newLabel + "\n";
    }

    // Convert lat, lon to relative position
    // usage: target.position = MainCamera.position + targetHandler.GPStoNAV(coordsGPS);
    public Vector3 GPStoNav(float[] coordsGPS)
    {
        // userCoords must be updated to come from telemetry stream!
        // userCoords = NavToGPS(user);
        // Debug.Log(telemetryData.getLatitude());

        double userLat = telemetryData.getLatitude();
        double userLon = telemetryData.getLongitude();

        // print(userCoords);

        float cEarthLat = 40075017.0f * Mathf.Cos(29.565f * Mathf.PI / 180.0f); // m
        float cEarthPol = 40007863.0f; // m

        float up = 0.0f;
        float east = (coordsGPS[1] - (float)userLon) * cEarthLat / 360.0f;
        float north = (coordsGPS[0] - (float)userLat) * cEarthPol / 360.0f;
        return new Vector3(east,up,north);

    }


    // Convert unity player coordinates to GPS coordinates for debugging purposes
    public float[] NavToGPS(Transform objectTransform)
    {
        // userCoords;
        // float[] originGPS = { 29.564331f, -95.081373f };
        float[] originGPS = { 29.564858f, -95.081765f }; // just south of station A
        
        float cEarthLat = 40075017.0f * Mathf.Cos(29.565f * Mathf.PI / 180.0f); // m
        float cEarthPol = 40007863.0f; // m

        float lon = originGPS[1] + (objectTransform.position.x) * 360.0f / cEarthLat;
        float lat = originGPS[0] + (objectTransform.position.z) * 360.0f / cEarthPol;
        return new float[] {lat, lon};

    }

}
