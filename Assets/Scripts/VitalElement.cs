using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class VitalElement : MonoBehaviour
{
    
	public Color dangerColor = Color.red;
    public Color cautionColor = Color.yellow;
    public Color goodColor = Color.green;

    public bool isMain = false;

    public string m_DataTitle;
    public float m_DataValue;
    public string m_DataUnitName;

    public float upperSafeLimit; 
    public float lowerSafeLimit; 

    private TextMeshPro m_BiometricTitle;
    private TextMeshPro m_BiometricValue;

    // Start is called before the first frame update
    void Start()
    {
    	if (isMain) {
    		m_BiometricValue = transform.GetChild(1).gameObject.GetComponent<TextMeshPro>();
    	} 


    	else {
    		m_BiometricTitle = transform.GetChild(0).gameObject.GetComponent<TextMeshPro>();
            m_BiometricValue = transform.GetChild(1).gameObject.GetComponent<TextMeshPro>(); 
    	}
    }

    // Update is called once per frame
    void Update()
    {
    	if (m_BiometricValue != null)
        {
            m_BiometricValue.color = GetCurrentColor();

            if (isMain == false && m_BiometricTitle != null) {
            	m_BiometricTitle.color = GetCurrentColor();
            }
        }
        
    }

    public void SetData(string dataTitle, float dataValue, float lower, float upper, string unit) {
        if (m_BiometricTitle == null || m_BiometricValue == null) return;

        if (isMain == false) {
        	m_BiometricTitle.text = dataTitle;
        }
        m_BiometricValue.text = dataValue.ToString() + unit;
        upperSafeLimit = upper;
        lowerSafeLimit = lower;

        m_DataValue = dataValue; 
        m_DataUnitName = unit; 
    }

    public Color GetCurrentColor()
    {
        if (m_DataValue > upperSafeLimit)
        {
            return dangerColor;
        }
        else if (m_DataValue <= lowerSafeLimit)
        {
            return dangerColor;
        }
        else
        {
            return goodColor;
        }
    }

}
