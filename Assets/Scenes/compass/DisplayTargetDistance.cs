using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DisplayTargetDistance : MonoBehaviour
{
    public GameObject Camera;
    public GameObject target;

    public string label = "label"; 
    public GameObject display;

    private TextMeshPro distText;

    // Start is called before the first frame update
    void Start()
    {
        distText = display.GetComponent<TextMeshPro>();
    }

    // Update is called once per frame
    void Update()
    {
        float dist = Vector3.Distance(Camera.transform.position, target.transform.position);
        distText.text = label + "\n" + (dist / 1.0f).ToString("0.0") + "m";
    }
}
