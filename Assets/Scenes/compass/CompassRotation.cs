using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompassRotation : MonoBehaviour
{
    public GameObject Compass;
    public GameObject Camera;

    private Vector3 currentEulerAngles;
    private float northCalibration = 0.0f;

    // Initialize Compass Orientation
    void Start()
    {
        // Compass.transform.Rotate(0.0f, 0.0f, 0.0f, Space.Self)
    }

    // Update is called once per frame
    void Update()
    {
        currentEulerAngles = Compass.transform.localEulerAngles;
        currentEulerAngles.y = -Camera.transform.localEulerAngles.y + northCalibration;
        Compass.transform.localEulerAngles = currentEulerAngles;
    }

    public void ResetNorth()
    {
        northCalibration = -Compass.transform.localEulerAngles.y;
    }
}
