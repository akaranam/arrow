using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompassTarget : MonoBehaviour
{
    public GameObject CompassElement;
    public GameObject Camera;
    public Transform Target;

    Vector3 currentEulerAngles;
    Vector3 targetDirection;

    // Initialize Compass Orientation
    void Start()
    {
        // Compass.transform.Rotate(0.0f, 0.0f, 0.0f, Space.Self)
    }

    // Update is called once per frame
    void Update()
    {
        targetDirection = Target.position - Camera.transform.position;
        targetDirection = Camera.transform.InverseTransformVector(targetDirection);

        currentEulerAngles = CompassElement.transform.localEulerAngles;
        currentEulerAngles.y = Mathf.Atan2(targetDirection.x, targetDirection.z) * Mathf.Rad2Deg;
        CompassElement.transform.localEulerAngles = currentEulerAngles;
    }
}
