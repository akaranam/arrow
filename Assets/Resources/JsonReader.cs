using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class JsonReader : MonoBehaviour
{
    public TextAsset textJson;

    public static T ImportJson<T>(string path)
    {
        TextAsset textAsset = Resources.Load<TextAsset>(path);
        return JsonUtility.FromJson<T>(textAsset.text);
    }

    [System.Serializable]
    public class MissionText {
        public string name;
        public string loc;
        public string desc;
    
    }

    [System.Serializable]
    public class MissionTextArray
    {
        public MissionText[] missions;

    }


    public MissionTextArray sampleObj;
    void ReadTest()
    {
        sampleObj = ImportJson<MissionTextArray>("missioninfo");
        Debug.Log(sampleObj.missions[2].name);
    }
    
    void Start()
    {
        ReadTest();
        /*Working
         * Location loc1 = new Location();
        loc1.name = "Mountain";
        loc1.x = 0;
        loc1.y = 1;

        string json2 = JsonUtility.ToJson(loc1);
        Debug.Log(json2);
        Location loc2 = JsonUtility.FromJson<Location>(json2);
        Debug.Log(loc2.name);*/

    }

}
