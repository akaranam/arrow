using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using System.IO;

public class InformationDisplayer : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        ReadTest();
    }

    //Json to Object
    public static T ImportJson<T>(string path)
    {
        TextAsset textAsset = Resources.Load<TextAsset>(path);
        //Debug.Log(textAsset.ToString());
        //string converted = textAsset.ToString();
        //TextAsset newConvert = ConvertStringToTextAsset(converted);
        //return JsonUtility.FromJson<T>(textAsset.text);
        return JsonUtility.FromJson<T>(textAsset.text);

    }

    /*public static TextAsset ConvertStringToTextAsset(string text)
    {
        string temporaryTextFileName = "TemporaryTextFile";
        File.WriteAllText(Application.dataPath + "/Resources/" + temporaryTextFileName + ".txt", text);
        //AssetDatabase.SaveAssets();
        //AssetDatabase.Refresh();
        TextAsset textAsset = Resources.Load(temporaryTextFileName) as TextAsset;
        return textAsset;
    }*/


    //Json to MissionTextArray Object
    public void ReadTest()
    {
        missionInfo = ImportJson<MissionTextArray>("missioninfo");
        geoInfo = ImportJson<GeoTextArray>("geoinfo");



    }

    //Mission Object Classes
    [System.Serializable]
    public class MissionText
    {
        public string name;
        public string loc;
        public string desc;

    }

    [System.Serializable]
    public class MissionTextArray
    {
        public MissionText[] missions;

    }

    //Geo Object Classes
    [System.Serializable]
    public class GeoObject
    {
        public string nameGeo;
        public string sizeShape;
        public string texture;
        public string majorComp;
        public string otherFeature;
        public string geoInterp;


    }

    [System.Serializable]
    public class GeoTextArray
    {
        public GeoObject[] geoObjects;

    }

    //Action Functions on Button Clicked
    public MissionTextArray missionInfo;
    

    public TextMeshPro name;
    public TextMeshPro objective;
    public TextMeshPro information;
    public GameObject nextUI;
    public GameObject navigationMenu;

    //Geo Variables
    public GeoTextArray geoInfo;
    public TextMeshPro nameGeo;
    public TextMeshPro sizeShape;
    public TextMeshPro texture;
    public TextMeshPro majorComp;
    public TextMeshPro otherFeature;
    public TextMeshPro geoInterp;

    //Add new geo variables
    public GameObject rock4;
    public GameObject geoProperties;
    public GameObject rockType;
    public GameObject addNewModule;




    public void OnMissionClicked(GameObject sender)
    {
        int index = 0;
        index = int.Parse(sender.tag);
        name.SetText(missionInfo.missions[index].name);
        objective.SetText(missionInfo.missions[index].loc);
        information.SetText(missionInfo.missions[index].desc);
    }


    public void OnGeoClicked(GameObject sender) {
        int index = 0;
        index = int.Parse(sender.tag);
        nameGeo.SetText(geoInfo.geoObjects[index].nameGeo);
        sizeShape.SetText(geoInfo.geoObjects[index].sizeShape);
        texture.SetText(geoInfo.geoObjects[index].texture);
        majorComp.SetText(geoInfo.geoObjects[index].majorComp);
        otherFeature.SetText(geoInfo.geoObjects[index].otherFeature);
        geoInterp.SetText(geoInfo.geoObjects[index].geoInterp);
    }

    public void AddNew(GameObject sender)
    {
        Debug.Log("pressed");
        rock4.SetActive(true);
        geoProperties.SetActive(false);
        addNewModule.SetActive(true);
        rockType.SetActive(true);
        sender.SetActive(false);
        
       
    }


    public void SwitchScene() {
        nextUI.SetActive(true);
        navigationMenu.SetActive(false);
    }
}
