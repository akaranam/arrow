using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.MixedReality.Toolkit.Input;

public class NewGeoController : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        ColorCube();
    }

    public int addGeoModuleState = 0;
    public GameObject rockType;
    public void RockTypeClick(GameObject sender) {
        Debug.Log("clicked");
        if (sender.tag == "GeolithButton")
        {
            addGeoModuleState = 2;
        }
        else {
            addGeoModuleState = 1;
        }
        geoStates[addGeoModuleState].SetActive(true);
        rockType.SetActive(false);
 
    }

    public GameObject[] geoStates;

    public void NextButton() {
        addGeoModuleState++;
        geoStates[addGeoModuleState].SetActive(true);
        geoStates[addGeoModuleState-1].SetActive(false);

    }
    public void Save() { 

    }

    public void TurnGreen(GameObject sphere) {
        //Get the Renderer component from the new cube
        var sphereRenderer = sphere.GetComponent<Renderer>();

        //Call SetColor using the shader property name "_Color" and setting the color to red
        sphereRenderer.material.SetColor("_Color", Color.green);

    }
    public GameObject cube;
    public GameObject r;
    public GameObject g;
    public GameObject b;
    Microsoft.MixedReality.Toolkit.UI.PinchSlider rVal;
    Microsoft.MixedReality.Toolkit.UI.PinchSlider bVal;
    Microsoft.MixedReality.Toolkit.UI.PinchSlider gVal;


    public void ColorCube()
    {
        //Get the Renderer component from the new cube
        rVal = r.GetComponent<Microsoft.MixedReality.Toolkit.UI.PinchSlider>();
        gVal = g.GetComponent<Microsoft.MixedReality.Toolkit.UI.PinchSlider>();
        bVal = b.GetComponent<Microsoft.MixedReality.Toolkit.UI.PinchSlider>();
        Debug.Log(rVal.SliderValue);
        //.get()
        var cubeRenderer = cube.GetComponent<Renderer>();

        //Call SetColor using the shader property name "_Color" and setting the color to red
        Color c = new Color(rVal.SliderValue, gVal.SliderValue, bVal.SliderValue);
        cubeRenderer.material.SetColor("_Color", c);

    }

}
