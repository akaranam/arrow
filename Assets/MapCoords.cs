using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Pixelplacement;

public class MapCoords : MonoBehaviour
{

    public string[] coordStrings2; //= new string[0];
    public List<float[]> coords2 = new List<float[]>();

    public Transform mapObj;
    public Transform spawnerPrefab;

    public LineRenderer line;
    public Transform pos1;
    public Transform pos2;
    public Transform refPosD;
    public Transform refPosG;


    public List<float[]> stationCoords = new List<float[]>();

    private float ratioLat;
    private float ratioLon;

    private Vector3 stationAmap;
    private Vector3 stationBmap;
    private Vector3 stationCmap;
    private Vector3 stationDmap;
    private Vector3 stationEmap;
    private Vector3 stationFmap;
    private Vector3 stationGmap;
    private Vector3 stationHmap;
    private Vector3 stationImap;

    private float[] stationA = { 29.5648150f, -95.0817410f };
    private float[] stationB = { 29.5646824f, -95.0811564f };
    private float[] stationC = { 29.5650460f, -95.0810944f };
    private float[] stationD = { 29.5645430f, -95.0816440f };
    private float[] stationE = { 29.5648290f, -95.0813750f };
    private float[] stationF = { 29.5647012f, -95.0813750f };
    private float[] stationG = { 29.5651359f, -95.0807408f };
    private float[] stationH = { 29.5651465f, -95.0814092f };
    private float[] stationI = { 29.5648850f, -95.0808360f };

    // private float[] userCoords;

    public GameObject navBeacon;
    public TargetHandler targetHandler;
    public Transform user;
    public TelemetryStream telemetryData;


    private void Start()
    {

        ratioLat = (refPosG.localPosition.y - refPosD.localPosition.y) / (stationG[0] - stationD[0]); // / 0.7171f; // unity / geo
        ratioLon = (refPosG.localPosition.x - refPosD.localPosition.x) / (stationG[1] - stationD[1]); // / 0.8284f; // unity / geo

        stationAmap = new Vector3(refPosD.localPosition.x + ratioLon * (stationA[1] - stationD[1]), refPosD.localPosition.y + ratioLat * (stationA[0] - stationD[0]), 3.5f);
        stationBmap = new Vector3(refPosD.localPosition.x + ratioLon * (stationB[1] - stationD[1]), refPosD.localPosition.y + ratioLat * (stationB[0] - stationD[0]), 3.5f);
        stationCmap = new Vector3(refPosD.localPosition.x + ratioLon * (stationC[1] - stationD[1]), refPosD.localPosition.y + ratioLat * (stationC[0] - stationD[0]), 3.5f);
        stationDmap = new Vector3(refPosD.localPosition.x + ratioLon * (stationD[1] - stationD[1]), refPosD.localPosition.y + ratioLat * (stationD[0] - stationD[0]), 3.5f);
        stationEmap = new Vector3(refPosD.localPosition.x + ratioLon * (stationE[1] - stationD[1]), refPosD.localPosition.y + ratioLat * (stationE[0] - stationD[0]), 3.5f);
        stationFmap = new Vector3(refPosD.localPosition.x + ratioLon * (stationF[1] - stationD[1]), refPosD.localPosition.y + ratioLat * (stationF[0] - stationD[0]), 3.5f);
        stationGmap = new Vector3(refPosD.localPosition.x + ratioLon * (stationG[1] - stationD[1]), refPosD.localPosition.y + ratioLat * (stationG[0] - stationD[0]), 3.5f);
        stationHmap = new Vector3(refPosD.localPosition.x + ratioLon * (stationH[1] - stationD[1]), refPosD.localPosition.y + ratioLat * (stationH[0] - stationD[0]), 3.5f);
        stationImap = new Vector3(refPosD.localPosition.x + ratioLon * (stationI[1] - stationD[1]), refPosD.localPosition.y + ratioLat * (stationI[0] - stationD[0]), 3.5f);


        stationCoords.Add(stationA);
        stationCoords.Add(stationB);
        stationCoords.Add(stationC);
        stationCoords.Add(stationD);
        stationCoords.Add(stationE);
        stationCoords.Add(stationF);
        stationCoords.Add(stationG);
        stationCoords.Add(stationH);
        stationCoords.Add(stationI);

        //int count = 0;

        //foreach (string coordString in coordStrings2)
        //{
        //    float coordX = 0;
        //    float coordY = 0;
        //    float.TryParse(coordString.Substring(0, 4), out coordX);
        //    float.TryParse(coordString.Substring(5, 4), out coordY);

        //    float[] coordXY = new float[2];
        //    coordXY[0] = coordX;
        //    coordXY[1] = coordY;

        //    coords2.Add(coordXY);

        //    float manualOffset = 0.3f;
        //    spawnerPrefab.localPosition = new Vector3(coords2[count][0] - manualOffset, coords2[count][1] - manualOffset, mapObj.localPosition.z);
        //    spawnerPrefab.localScale = new Vector3(0.05f, 0.05f, 0.05f);
        //    //Instantiate(spawnerPrefab, mapObj);

        //    count++;

        //    print(coords2[0][0]);


        //}

        //line.positionCount = 2;

        //print("COORDS: " + coords2.Count);
    }

    private void Update()
    {
        // update user position
        // userCoords = targetHandler.NavToGPS(user);

        double userLat = telemetryData.getLatitude();
        double userLon = telemetryData.getLongitude();

        // print(userCoords);
        pos1.localPosition = new Vector3(refPosD.localPosition.x + ratioLon * ((float)userLon - stationD[1]), refPosD.localPosition.y + ratioLat * ((float)userLat - stationD[0]), 3.5f);
        if(!navBeacon.activeInHierarchy)
        {
            pos2.localPosition = pos1.localPosition + new Vector3(0.0f, 0.0f, 0.001f);
        }

        // update line renderer in real time
        line.SetPosition(0, pos1.position);
        line.SetPosition(1, pos2.position);

        //print("Update Function Called");
        //print("Line Pt A: " + coords2[0][0].ToString());
        //print("Line Pt A: " + coordTransforms[0].localPosition.x + ", " + coordTransforms[0].localPosition.y);
        //print("Line Pt B: " + coordTransforms[1].localPosition.x + ", " + coordTransforms[1].localPosition.y);
        //line.SetPosition(0, new Vector3(coordTransforms[0].localPosition.x, coordTransforms[0].localPosition.y, mapObj.position.z));
        //line.SetPosition(1, new Vector3(coordTransforms[1].localPosition.x, coordTransforms[1].localPosition.y, mapObj.position.z));

    }


    public void SetCoords(string mission)
    {
        navBeacon.SetActive(true);

        if (mission == "A")        // Mission A selected
        {
            Tween.LocalPosition(pos2, stationAmap, 0.3f, 0);
            targetHandler.SetTargetCoords(stationA);
            targetHandler.SetTargetLabel("station A");
        }
        else if (mission == "B")   // Mission B selected
        {
            Tween.LocalPosition(pos2, stationBmap, 0.3f, 0);
            targetHandler.SetTargetCoords(stationB);
            targetHandler.SetTargetLabel("station B");
        }
        else if (mission == "C")   // Mission C selected
        {
            Tween.LocalPosition(pos2, stationCmap, 0.3f, 0);
            targetHandler.SetTargetCoords(stationC);
            targetHandler.SetTargetLabel("station C");
        }
        else if (mission == "D")   // Mission D selected
        {
            Tween.LocalPosition(pos2, stationDmap, 0.3f, 0);
            targetHandler.SetTargetCoords(stationD);
            targetHandler.SetTargetLabel("station D");
        }
        else if (mission == "E")   // Mission E selected
        {
            Tween.LocalPosition(pos2, stationEmap, 0.3f, 0);
            targetHandler.SetTargetCoords(stationE);
            targetHandler.SetTargetLabel("station E");
        }
        else if (mission == "F")   // Mission F selected
        {
            Tween.LocalPosition(pos2, stationFmap, 0.3f, 0);
            targetHandler.SetTargetCoords(stationF);
            targetHandler.SetTargetLabel("station F");
        }
        else if (mission == "G")   // Mission G selected
        {
            Tween.LocalPosition(pos2, stationGmap, 0.3f, 0);
            targetHandler.SetTargetCoords(stationG);
            targetHandler.SetTargetLabel("station G");
        }
        else if (mission == "H")   // Mission H selected
        {
            Tween.LocalPosition(pos2, stationHmap, 0.3f, 0);
            targetHandler.SetTargetCoords(stationH);
            targetHandler.SetTargetLabel("station H");
        }
        else if (mission == "I")   // Mission I selected
        {
            Tween.LocalPosition(pos2, stationImap, 0.3f, 0);
            targetHandler.SetTargetCoords(stationI);
            targetHandler.SetTargetLabel("station I");
        }
    }



    //// Remove element from an array
    //private void RemoveElement<T>(ref T[] theA, int theOne)
    //{
    //    for (int i=theOne; i<theA.Length; i++)
    //    {
    //        theA[i] = theA[i + 1];
    //    }

    //    Array.Resize(ref theA, theA.Length - 1);
    //}


}
