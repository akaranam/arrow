using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System;
using System.IO;

/// <summary>
/// Recieves and parses telemetry data from a server. 
/// </summary>

// mirror json code into locationJSON
//  - start another coroutine and create copy of RunWWW.
// write locationJSON into a file
// read from file into getter methods


public class TelemetryStream : MonoBehaviour {

    public string m_JSONString;
    [SerializeField] private LocationData _LocationData = new LocationData();

    [Header("Server URLS")]
    public string url = ""; //INSERT URL HERE
    public string locationURL = "";
    public const int OBJECTID_LENGTH = 47;
    private bool m_bGettingSuitData = false;

    [Header("Display Text")]
    public Text bioText;

    public VitalElement[] m_SuitDataUIElements;

    // public Text timeLeftText;

    [Space(10)]

    // Expected ranges for telemetry data 
    [Header("Ranges")]

    [Space(5)]
    public float m_HeartRateLow;
    public float m_HeartRateHigh;

    [Space(5)]
    public float m_P_SuitLow;
    public float m_P_SuitHigh;

    [Space(5)]
    public float m_P_SubLow;
    public float m_P_SubHigh;

    [Space(5)]
    public float m_T_SubLow;
    public float m_T_SubHigh;

    [Space(5)]
    public float m_V_FanLow;
    public float m_V_FanHigh;

    [Space(5)]
    public float m_P_O2Low;
    public float m_P_O2High;

    [Space(5)]
    public float m_Rate_O2Low;
    public float m_Rate_O2High;

    [Space(5)]
    public float m_Cap_BatteryLow;
    public float m_Cap_BatteryHigh;

    [Space(5)]
    public float m_P_H2O_GLow;
    public float m_P_H2O_GHigh;

    [Space(5)]
    public float m_P_H2O_LLow;
    public float m_P_H2O_LHigh;

    [Space(5)]
    public float m_P_SOPLow;
    public float m_P_SOPHigh;

    [Space(5)]
    public float m_Rate_SOPLow;
    public float m_Rate_SOPHigh;

    bool weCanQuit = false; 

    void Start ()
    {
        //StartCoroutine(RunStartWWW());
        InvokeRepeating("UpdateSystemData", 1.0f, 1);
    }

    private void OnApplicationQuit ()
    {
        /*
        while(!weCanQuit)
        {
            //StartCoroutine(RunStopWWW()); 
        }
        */
    }

    void testFunction() {
        Debug.Log("testing! testing!");
    }

    private void UpdateSystemData()
    {
        //StartCoroutine(RunWWW());
        StartCoroutine(RunLocation()); 
    }

    IEnumerator RunWWW()
    {
        // TODO: MIRROR ALL "json" code to "locationURL"
        if (m_bGettingSuitData == true) yield break;

        using (UnityWebRequest www = UnityWebRequest.Get(url))
        {
            yield return www.SendWebRequest();

            m_bGettingSuitData = true;
            string json = ""; 
            if (www.isNetworkError)
            {

            }
            else if (www.isHttpError)
            {
            }
            else 
            {
                json = www.downloadHandler.text;
                Debug.Log("Connected to the vitals telemetry stream");
            }

            if (!json.Equals(""))
            {
                SuitData jsonObject = JsonUtility.FromJson<SuitData>(json);
                Debug.Log("Parsing vitals");
                UpdateUI(jsonObject);       
                m_bGettingSuitData = false;
            }
            else
            {
                Debug.Log("no data recieved from the server");
                m_bGettingSuitData = false;

            }
        }
    }

    public double getLatitude() {
        string jsonString = File.ReadAllText("Assets/locationData.json"); 
        return JsonUtility.FromJson<LocationData>(jsonString).latitude;
    }

    public double getLongitude() {
        string jsonString = File.ReadAllText("Assets/locationData.json"); 
        return JsonUtility.FromJson<LocationData>(jsonString).longitude;
    }

    IEnumerator RunLocation()
    {
        // TODO: MIRROR ALL "json" code to "locationURL"
        if (m_bGettingSuitData == true) yield break;

        using (UnityWebRequest www = UnityWebRequest.Get(locationURL))
        {
            yield return www.SendWebRequest();

            m_bGettingSuitData = true;
            string json = ""; 
            if (www.isNetworkError)
            {
            }
            else if (www.isHttpError)
            {
            }
            else 
            {
                json = www.downloadHandler.text;
                Debug.Log("Connected to the location telemetry stream");
            }

            if (!json.Equals(""))
            {  
                SuitLocation jsonObject = JsonUtility.FromJson<SuitLocation>(json.Substring(1, json.Length - 2));
      
                // Debug.Log("Parsing Location");
                // Debug.Log("Latitude");
                // Debug.Log(getLatitude());
                // Debug.Log("Longitude");
                // Debug.Log(getLongitude());
                _LocationData.latitude = jsonObject.latitude;
                _LocationData.longitude = jsonObject.longitude; 
                string location = JsonUtility.ToJson(_LocationData);
                System.IO.File.WriteAllText("Assets/locationData.json", location);
                m_bGettingSuitData = false;

            }
            else
            {
                Debug.Log("no data recieved from the server");
                m_bGettingSuitData = false;

            }
        }
    }

    
    private void UpdateUI(SuitData data)
    {
        //Current just get hours, could be better 
        float waterHours = float.Parse(data.t_water.Split(':')[0]);
        float oxygenHours = float.Parse(data.t_oxygen.Split(':')[0]);
        float batteryHours = float.Parse(data.t_battery.Split(':')[0]);

        m_SuitDataUIElements[1].SetData("Time Life Battery", batteryHours, 1.0f, 10.0f, "hrs");
        m_SuitDataUIElements[2].SetData("Time Life Oxygen", oxygenHours, 1.0f, 10.0f, "hrs");
        m_SuitDataUIElements[3].SetData("Time Life Water", waterHours, 1.0f, 10.0f, "hrs");

        m_SuitDataUIElements[4].SetData("1st O2", data.ox_primary, 20.0f, 101.0f, "%");
        m_SuitDataUIElements[5].SetData("Heart Rate", data.heart_bpm, 80.0f, 100.0f, "bpm");
        m_SuitDataUIElements[6].SetData("SUB Pres", data.p_sub, 2.0f, 4.0f, "psia");
        m_SuitDataUIElements[7].SetData("SUB Temp", data.t_sub, -148.0f, 248.0f, "° F");

        m_SuitDataUIElements[8].SetData("Suit Pres", data.p_suit, 2.0f, 4.0f, "psid");
        m_SuitDataUIElements[9].SetData("Fan Tachometer", data.v_fan, 10000.0f, 40000.0f, "RPM");

        m_SuitDataUIElements[10].SetData("Oxygen Pressure", data.p_o2, 750.0f, 950.0f, "psia");
        m_SuitDataUIElements[11].SetData("Oxygen Rate", data.rate_o2, 0.5f, 1.0f, "psi/min");

        m_SuitDataUIElements[13].SetData("H20 Gas Pressure", data.p_h2o_g, 14.0f, 16.0f, "psia");
        m_SuitDataUIElements[14].SetData("H20 Liquid Pressure", data.p_h2o_l, 14.0f, 16.0f, "psia");
        m_SuitDataUIElements[15].SetData("SOP Pressure", data.p_sop, 750.0f, 950.0f, "psia");
        m_SuitDataUIElements[16].SetData("SOP Rate", data.rate_sop, 0.5f, 1.0f, "psi/min");


        Debug.Log(data.createdAt);
    }

}


//////////////////////// All telemetry variables are defined here /////////////////////////////////
[System.Serializable]
public class SuitData
{
    public int id; 
    public int room;
    public bool isRunning;
    public bool isPaused;
    public int time; 
    public string timer;
    public string started_at;
    public int heart_bpm;
    public int p_sub = 0;
    public int p_suit = 0;
    public int t_sub = 0;
    public int v_fan = 0;
    public int p_o2 = 0;
    public int rate_o2 = 0;
    public int batteryPercent = 100;
    public int cap_battery = 0;
    public int battery_out = 0;
    public int p_h2o_g = 0;
    public int p_h2o_l = 0;
    public int p_sop = 0;
    public int rate_sop = 0;
    //If something breaks, check this 
    public string t_battery = "00:00:00";
    public int t_oxygenPrimary = 100;
    public int t_oxygenSec = 100;
    public int ox_primary = 100;
    public int ox_secondary = 100;
    public string t_oxygen = "00:00:00";
    public int cap_water = 100;
    public string t_water = "00:00:00";
    public string createdAt;
    public string updatedAt = "2022-05-19T20:30:54.771Z";
}

[System.Serializable]
public class SuitLocation 
{
    public int id = 2;
    public int user; 
    public int room;
    public double latitude = 0;
    public double longitude = 0;
    public double altitude; 
    public double accuracy; 
    public double altitudeAccuracy; 
    public double heading;
    public double speed;
    public string createdAt;
    public string updatedAt; 
}

public class LocationData {
    public double latitude;
    public double longitude;
}