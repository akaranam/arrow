using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideAndShow : MonoBehaviour
{
    public GameObject persistentCompass;
    public GameObject handMapToggle;

    public void toggle() {
        persistentCompass.SetActive(!persistentCompass.activeSelf);
        handMapToggle.SetActive(!handMapToggle.activeSelf);
    }

}
